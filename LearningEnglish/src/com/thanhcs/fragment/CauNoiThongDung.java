package com.thanhcs.fragment;

import java.util.ArrayList;

import com.thanhcs.adapter.ArrayAdapterSpiner;
import com.thanhcs.adapter.FiveWorldArrayAdapter;
import com.thanhcs.adapter.FullWordArrayAdapter;
import com.thanhcs.adapter.PharseArrayAdapter;
import com.thanhcs.adapter.RegularArrayAdapter;
import com.thanhcs.database.PharseDataBaseProcess;
import com.thanhcs.database.Phrases;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CauNoiThongDung extends Fragment{

	ListView lv;
	TextView tv;
	ArrayList<Phrases> arrPhrases = new ArrayList<Phrases>();
	public ArrayList<String> english;
	public ArrayList<String> vietnamese;
	Phrases [] phrases;
	Spinner spinner;
	String  [] titlecntd;
	Phrases myPhrases = new Phrases();
	PharseDataBaseProcess db;
	EditText editsearch;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.fullword, container, false);
		TextView tv2 =(TextView)rootView.findViewById(R.id.textView2);
		tv2.setVisibility(rootView.INVISIBLE);
		editsearch = (EditText)rootView.findViewById(R.id.textView1);
		editsearch.setVisibility(rootView.INVISIBLE);
		lv =(ListView)rootView.findViewById(R.id.lv);
		tv =(TextView)rootView.findViewById(R.id.textView1);
		
		db =  new PharseDataBaseProcess(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();


		english=  new ArrayList<String>();
		vietnamese=  new ArrayList<String>();
		spinner = (Spinner)rootView.findViewById(R.id.spiner);
		titlecntd = getResources().getStringArray(R.array.titlecntd);
		ArrayAdapterSpiner adp = new ArrayAdapterSpiner(getActivity().getApplicationContext(), titlecntd);
		adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adp);
		spinner.setOnItemSelectedListener((OnItemSelectedListener) new ProcessforSpinner());

		return rootView;
	}
	
	private class  ProcessforSpinner implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			Toast.makeText(getActivity().getApplicationContext(), ""+titlecntd[arg2], Toast.LENGTH_SHORT).show();
			switch (arg2) {
			case 0:
				arrPhrases = db.getListPhrases();
				for (int i = 0 ; i< arrPhrases.size();i++)
				{
					english.add(arrPhrases.get(i).getEnglish());
					vietnamese.add(arrPhrases.get(i).getVietnamese());
				}

				PharseArrayAdapter adt; //= new ArrayAdapter<Word>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, plist);
				adt = new PharseArrayAdapter(getActivity(), english, vietnamese);
				lv.setAdapter(adt);

				break;
			case 1:
				getloaicntd("nhungthanhnguthongdung");
				tv.setText(titlecntd[arg2]);
				break;
			case 2:
				getloaicntd("chaohoi");
				tv.setText(titlecntd[arg2]);
				
				break;
			case 3:
				getloaicntd("dulichphuonghuong");
				tv.setText(titlecntd[arg2]);
				break;
			case 4:
				getloaicntd("consotienbac");
				tv.setText(titlecntd[arg2]);
				break;
			case 5:
				getloaicntd("diadiem");
				tv.setText(titlecntd[arg2]);
				break;
			case 6:
				getloaicntd("dienthoaiinternetthu");
				break;
			case 7:
				getloaicntd("thoigianvangaythang");
				tv.setText(titlecntd[arg2]);
				break;
			case 8:
				getloaicntd("choano");
				tv.setText(titlecntd[arg2]);
				break;
			case 9:
				getloaicntd("an");
				tv.setText(titlecntd[arg2]);
				break;
			case 10:
				getloaicntd("ketban");
				tv.setText(titlecntd[arg2]);
				break;
			case 11:
				getloaicntd("giaitri");
				tv.setText(titlecntd[arg2]);
				break;
			case 12:
				getloaicntd("muasam");
				tv.setText(titlecntd[arg2]);
				break;
			case 13:
				getloaicntd("khokhangiaotiep");
				tv.setText(titlecntd[arg2]);
				break;
			case 14:
				getloaicntd("truonghopkhancapsuckhoe");
				tv.setText(titlecntd[arg2]);
				break;
			case 15:
				getloaicntd("cauhoithongdung");
				tv.setText(titlecntd[arg2]);
				break;
			case 16:
				getloaicntd("vieclam");
				tv.setText(titlecntd[arg2]);
				break;
			case 17:
				getloaicntd("thoitiet");
				tv.setText(titlecntd[arg2]);
				break;



			default:
				break;
			}
		}
		
		
		public void getloaicntd(String tag) {
			arrPhrases = new ArrayList<Phrases>();
			english = new ArrayList<String>();
			vietnamese = new ArrayList<String>();
			arrPhrases= db.getcntdloai(tag);
			for(int dem=0;dem<arrPhrases.size();dem++)
			{
				english.add(arrPhrases.get(dem).getEnglish());
				vietnamese.add(arrPhrases.get(dem).getVietnamese());
			}
			
			PharseArrayAdapter adt; //= new ArrayAdapter<Word>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, plist);
			adt = new PharseArrayAdapter(getActivity(), english, vietnamese);
			lv.setAdapter(adt);
						
		}		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}


	}

}
