package com.thanhcs.fragment;

import java.util.ArrayList;
import java.util.Locale;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.thanhcs.adapter.ArrayAdapterSpiner;
import com.thanhcs.adapter.FullWordArrayAdapter;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;

import android.app.Fragment;
import android.os.Bundle;
import android.sax.RootElement;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


public class FullWord extends Fragment{
	ListView lv;
	ArrayList<Word> arrWord = new ArrayList<Word>();
	ArrayList<Integer> id;
	ArrayList<Integer> learn;
	EditText editsearch;
	Spinner spinnerabc;
	@SuppressWarnings("rawtypes")
	ProcessWordDataBase db ;
	Word [] word;
	
	String []a ={"A", "B", "C", "D", "E",
			"F", "G", "H", "I", "J",
			"K", "L", "M", "N", "O",
			"P", "Q", "R", "S", "T",
			"W", "U", "V", "W", "Y",
			"Z"

	};


	@SuppressWarnings("rawtypes")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fullword, container, false);
		 
		
	    
		lv =(ListView)rootView.findViewById(R.id.lv);
		TextView tv2 =(TextView)rootView.findViewById(R.id.textView2);
		tv2.setVisibility(rootView.INVISIBLE);
		editsearch = (EditText)rootView.findViewById(R.id.textView1);
		spinnerabc =(Spinner)rootView.findViewById(R.id.spiner);
		db=  new ProcessWordDataBase(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();
		
		ArrayAdapterSpiner adpspin =new ArrayAdapterSpiner(getActivity().getApplicationContext(), a);
		adpspin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//R.layout.spinnerdemo
		spinnerabc.setAdapter(adpspin);
		spinnerabc.setOnItemSelectedListener(new ItemselectSP());







		return rootView;
	}
	
	
	
	
	  /** Called before the activity is destroyed. */
	  @Override
	  public void onDestroy() {
	    // Destroy the AdView.
		  db.close();
	    super.onDestroy();
	  }

	private class ItemselectSP implements OnItemSelectedListener
	{

		/* (non-Javadoc)
		 * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {

			if(a[arg2].equalsIgnoreCase(a[0])) //1->221
			{
				setkhiclickABC(1, 3114);//0
			}
			if(a[arg2].equalsIgnoreCase(a[1]))
			{
				setkhiclickABC(222, 370);//a
			}
			if(a[arg2].equalsIgnoreCase(a[2]))
			{
				setkhiclickABC(371, 691);//b

			}
			if(a[arg2].equalsIgnoreCase(a[3]))
			{
				setkhiclickABC(692, 887);//c
			}
			if(a[arg2].equalsIgnoreCase(a[4]))
			{
				setkhiclickABC(888, 1045);//d
			}
			if(a[arg2].equalsIgnoreCase(a[5]))
			{
				setkhiclickABC(1046,1200 );//e
			}
			if(a[arg2].equalsIgnoreCase(a[6])) //1->221
			{
				setkhiclickABC(1201, 1283);//f
			}
			if(a[arg2].equalsIgnoreCase(a[7]))
			{
				setkhiclickABC(1284, 1377);//g
			}
			if(a[arg2].equalsIgnoreCase(a[8]))
			{
				setkhiclickABC(1378, 1503);//h
			}
			if(a[arg2].equalsIgnoreCase(a[9]))
			{
				setkhiclickABC(1504, 1526);//i
			}
			if(a[arg2].equalsIgnoreCase(a[10]))
			{
				setkhiclickABC(1527, 1553);//j
			}
			if(a[arg2].equalsIgnoreCase(a[11]))
			{
				setkhiclickABC(1554, 1658);//k
			}
			if(a[arg2].equalsIgnoreCase(a[12])) //1->221
			{
				setkhiclickABC(1659, 1791);//l
			}
			if(a[arg2].equalsIgnoreCase(a[13]))
			{
				setkhiclickABC(1792, 1854);//m
			}
			if(a[arg2].equalsIgnoreCase(a[14]))
			{
				setkhiclickABC(1855, 1925);//n
			}
			if(a[arg2].equalsIgnoreCase(a[15]))
			{
				setkhiclickABC(1926, 2191);//o
			}
			if(a[arg2].equalsIgnoreCase(a[16]))
			{
				setkhiclickABC(2192, 2206);//p
			}
			if(a[arg2].equalsIgnoreCase(a[17]))
			{
				setkhiclickABC(2207, 2389);//q
			}
			if(a[arg2].equalsIgnoreCase(a[18])) //1->221
			{
				setkhiclickABC(2390, 2776);//r
			}
			if(a[arg2].equalsIgnoreCase(a[19]))
			{
				setkhiclickABC(2777, 2931);//s
			}
			if(a[arg2].equalsIgnoreCase(a[20]))
			{
				setkhiclickABC(2932, 2970);//t
			}
			if(a[arg2].equalsIgnoreCase(a[21]))
			{
				setkhiclickABC(2970, 3007);//u
			}
			if(a[arg2].equalsIgnoreCase(a[22]))
			{
				setkhiclickABC(3008, 3105);//v
			}
			if(a[arg2].equalsIgnoreCase(a[23]))
			{
				setkhiclickABC(3106, 3113);//w
			}
			if(a[arg2].equalsIgnoreCase(a[24])) //1->221
			{
				setkhiclickABC(3114, 3114);//x
			}



		}


		/* (non-Javadoc)
		 * @see android.widget.AdapterView.OnItemSelectedListener#onNothingSelected(android.widget.AdapterView)
		 */
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}



	}
	@SuppressWarnings("unchecked")
	private void setkhiclickABC(int i, int j) {
		arrWord =new ArrayList<Word>();
		arrWord = db.getListAbcWord(i, j);
		setlist();

	}
	private void setlist() {
		final FullWordArrayAdapter adt;
		adt = new FullWordArrayAdapter(getActivity(), arrWord);
		lv.setAdapter(adt);
		editsearch.addTextChangedListener(new TextWatcher() {
			//Event when changed word on EditTex
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//Lọc  array list voi nhưng phần tử là 1 word có word chứa ký tự trong EditTex
					ArrayList<Word> temp=new ArrayList<Word>();
					int textlength = editsearch.getText().length();
					temp.clear();
					for (int i = 0; i < arrWord.size(); i++)
					{
						if (textlength <= arrWord.get(i).getWord().length())
						{
							if(editsearch.getText().toString().equalsIgnoreCase(
									(String)
									arrWord.get(i).getWord().subSequence(0,
											textlength)))
							{
								temp.add(arrWord.get(i));
							}
						}
					}
					lv.setAdapter(new FullWordArrayAdapter(getActivity(),temp));
					
				}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

	}
}
