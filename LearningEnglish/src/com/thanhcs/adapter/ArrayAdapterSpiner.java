package com.thanhcs.adapter;

import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.learning3000english.*;

import android.content.Context;
import android.sax.RootElement;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterSpiner extends ArrayAdapter<String> {

	@SuppressWarnings("unused")
	private LayoutInflater inflater;
	String[] abc;
	Context context;
	TextView tv;
	public ArrayAdapterSpiner(Context context, String[] abc) {
		super(context, R.layout.spinnerdemo, abc);
		this.abc = abc;
		this.context = context;
		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public Context getContext() {
		return super.getContext();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public View getView(final int position,View convertView, ViewGroup parent) {
		convertView = inflater.inflate(R.layout.spinnerdemo, null);
		
		tv= (TextView) convertView.findViewById(R.id.tvabc);
		tv.setText(abc[position]);

		return convertView;
	}
}
