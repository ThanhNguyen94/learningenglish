package com.thanhcs.learning3000english;

import java.util.ArrayList;

import com.thanhcs.adapter.ArrayAdapterSpiner;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;






import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;


public class Activity_PracticeListen extends Activity {

	EditText ketqua;
	ImageButton btspeak;
	Button btcheck;
	ImageView imgv;
	public LayoutInflater inflater;
	Button btnext, btxem;
	TextView explain, tvchitiet;
	MediaPlayer mPlayer;
	Word testWord = new Word();
	ArrayList<Word> arrWord = new ArrayList<Word>();
	int demhienXem=0;
	Spinner spn;
	ProcessWordDataBase db ;
	String []a = {"Danh sach tu da hoc", "Ngau nhien tu 300 tu"};
	int randomNum =3 ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tracnghiemtuvung);

		btspeak = (ImageButton) findViewById(R.id.btspeak);
		btcheck = (Button) findViewById(R.id.btcheckdapan);
		ketqua  = (EditText)findViewById(R.id.edketqua);
		explain = (TextView)findViewById(R.id.tvhuongdan);
		btxem= (Button) findViewById(R.id.button1);
		btnext = (Button)findViewById(R.id.btnext);
		//=================get database====================
		db =  new ProcessWordDataBase(Activity_PracticeListen.this);
		db.createDatabase();
		db.open();
		arrWord =db.getTestImportant();
		//============================
		spn=(Spinner)findViewById(R.id.spinner1);
		ArrayAdapterSpiner adpspin =new ArrayAdapterSpiner(Activity_PracticeListen.this, a);
		adpspin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//R.layout.spinnerdemo
		spn.setAdapter(adpspin);
		spn.setOnItemSelectedListener(new ItemselectSP());
		//	randomNum = 1 + (int)(Math.random()*arrWord.size()-1); 
		imgv = (ImageView)findViewById(R.id.imageView2);
		final Animation animTranslate = AnimationUtils.loadAnimation(Activity_PracticeListen.this,
				R.anim.anim_translate);
		final Animation animAlpha = AnimationUtils.loadAnimation(Activity_PracticeListen.this,
				R.anim.anim_alpha);
		final Animation animScale = AnimationUtils.loadAnimation(Activity_PracticeListen.this,
				R.anim.anim_scale);
		final Animation animRotate = AnimationUtils.loadAnimation(Activity_PracticeListen.this,
				R.anim.anim_rotate);



		btnext.setVisibility(View.GONE);
		btxem.setVisibility(View.GONE);
		btcheck.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View view) {
				String text = ketqua.getText().toString();

				if(testWord.getWord().equalsIgnoreCase(text))
				{


					imgv.setImageResource(R.drawable.dung);
					imgv.startAnimation(animRotate);
					mPlayer = MediaPlayer.create (Activity_PracticeListen.this,R.raw.congattulaysan);
					mPlayer.start();
					btnext.setVisibility(View.VISIBLE);



				}

				else 
				{
					imgv.setImageResource(R.drawable.sai);
					imgv.startAnimation(animScale);
					mPlayer = MediaPlayer.create (Activity_PracticeListen.this,R.raw.tryanotheranswer);
					mPlayer.start();
					demhienXem = demhienXem+1;
					if(demhienXem==3)
					{
						btxem.setVisibility(View.VISIBLE);
						btxem.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								Toast.makeText(Activity_PracticeListen.this,""+ testWord.getWord() +"--"+testWord.getVidu(), Toast.LENGTH_LONG).show();
								demhienXem =0;	
						}
					});

				}
				
				else
				{

				}
			}

		}
	});
	

		btnext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				randomNum = 1 + (int)(Math.random()*arrWord.size()-1); 
				//	Toast.makeText(Activity_PracticeListen.this, testWord.getWord(), Toast.LENGTH_SHORT).show();
				btnext.setVisibility(View.GONE);
				btxem.setVisibility(View.GONE);
				ketqua.setText("");
				ketqua.setFocusable(true);
				imgv.setImageResource(R.drawable.binhthuong);
				//	Random random = new Random();
				//	 int randomInteger = random.nextInt(arrWord.size());
				testWord =  arrWord.get(randomNum);
			}	
		});




		btspeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				new com.thanhcs.database.TTS(testWord.getWord(),Activity_PracticeListen.this);
				//	Toast.makeText(Activity_PracticeListen.this, testWord.getWord(), Toast.LENGTH_SHORT).show();

			}
		});


}
private class ItemselectSP implements OnItemSelectedListener
{

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {

		if(a[arg2].equalsIgnoreCase(a[0])) //1->221
		{
			arrWord = db.getTestImportant();
			randomNum = 1 + (int)(Math.random()*arrWord.size()-2); 
			testWord = arrWord.get(randomNum);

		}

		if(a[arg2].equalsIgnoreCase(a[1])) //1->221
		{
			arrWord = db.getListAbcWord(1, 3114);
			randomNum = 1 + (int)(Math.random()*arrWord.size()-2); 
			testWord = arrWord.get(randomNum);

		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}
}
@Override
public void onDestroy() {
	db.close();
	super.onDestroy();
}
}
