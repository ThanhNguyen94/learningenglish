package com.thanhcs.fragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thanhcs.learning3000english.Activity_PracticeListen;
import com.thanhcs.learning3000english.Activity_SpeakRandom;
import com.thanhcs.learning3000english.R;
import com.thanhcs.learning3000english.ViewWordDetail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;

@SuppressLint("ValidFragment")
public class PractivePhrases extends Fragment implements OnClickListener{
	
	private AdView adView;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.practive_word, container, false);
		adView = (AdView)rootView.findViewById(R.id.adView);
		adView.loadAd(new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)      
	    .addTestDevice("85AFA7708F0526A57D3599F39F71656D").build());


		
		final Animation animTranslate = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_translate);
		final Animation animAlpha = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_alpha);
		final Animation animScale = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_scale2);
		final Animation animRotate = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_rotate);
		Button btlisten, btspeak, btphraseseng;
		btlisten = (Button)rootView.findViewById(R.id.btlisten);
		btspeak = (Button)rootView.findViewById(R.id.btspeak);
		btlisten.setOnClickListener(this);
		btspeak.setOnClickListener(this);
		btlisten.startAnimation(animTranslate);
		btspeak.startAnimation(animAlpha);

		return rootView;
	}

	@Override
	public void onPause() {
        adView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    public void onDestroy() {
        adView.destroy();
        super.onDestroy();
    }
    
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btlisten:
		{
			Intent intent = new Intent(getActivity().getApplicationContext(), Activity_PracticeListen.class);
			startActivity(intent);
		}
			
			break;

		case R.id.btspeak:
		{
			Intent intent = new Intent(getActivity().getApplicationContext(), Activity_SpeakRandom.class);
			startActivity(intent);
		}
			
			break;
			
		default:
			break;
		}
		
	}
	
}
