package com.thanhcs.fragment;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thanhcs.learning3000english.R;

public class RattingApp extends Fragment {
	private AdView adView;
	
	public RattingApp() {
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_two, container,
				false);
		adView = (AdView)view.findViewById(R.id.adView);
		adView.loadAd(new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)      
	    .addTestDevice("85AFA7708F0526A57D3599F39F71656D").build());

		startActivity(new Intent(Intent.ACTION_VIEW, 
			    Uri.parse("https://play.google.com/store/apps/details?id=com.thanhcs.learning3000english")));
		return view;
	}
	@Override
	public void onPause() {
        adView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    public void onDestroy() {
        adView.destroy();
        super.onDestroy();
    }


}
