package com.thanhcs.fragment;

import java.util.ArrayList;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thanhcs.adapter.FullWordArrayAdapter;
import com.thanhcs.adapter.RegularArrayAdapter;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Regular;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.Activity_Regular_View;
import com.thanhcs.learning3000english.R;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class DongTuBatQuyTac extends Fragment {
	
	ProcessWordDataBase db;
	ListView lv;
	ArrayList<String> arr1 = new ArrayList<String>();
	ArrayList<String> arr2 = new ArrayList<String>();
	ArrayList<String> arr3 = new ArrayList<String>();
	ArrayList<String> tonghop = new ArrayList<String>();
	ArrayList<String> nghia = new ArrayList<String>();
	ArrayList<String> vidu = new ArrayList<String>();
	ArrayList<Regular> regular = new ArrayList<Regular>();
	EditText editsearch;
	
	
	Spinner spn;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fullword, container, false);
		
		

		
		lv = (ListView)rootView.findViewById(R.id.lv);
		TextView tv2 =(TextView)rootView.findViewById(R.id.textView2);
		tv2.setVisibility(rootView.INVISIBLE);
		spn =(Spinner)rootView.findViewById(R.id.spiner);
		spn.setVisibility(rootView.INVISIBLE);
		editsearch = (EditText)rootView.findViewById(R.id.textView1);
		db = new ProcessWordDataBase(getActivity().getApplicationContext());
		db.open();
		regular = db.getRegular();
		for(int i=0;i<regular.size();i++)
		{
			arr1.add(regular.get(i).getInfinitive());
			arr2.add(regular.get(i).getPast_simple());
			arr3.add(regular.get(i).getPast_paticiple());
			nghia.add(regular.get(i).getVi_content());
			vidu.add(regular.get(i).getExample());
		}
		for(int i=0;i<arr1.size();i++)
		{
			tonghop.add(arr1.get(i)+" - "+arr2.get(i)+" - "+ arr3.get(i));
		}
		
		
		RegularArrayAdapter adp = new RegularArrayAdapter(getActivity(),tonghop,nghia, vidu);
		//ArrayAdapter<String> adt =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tonghop);
		lv.setAdapter(adp);
		
		editsearch.addTextChangedListener(new TextWatcher() {
			//Event when changed word on EditTex
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//Lọc  array list voi nhưng phần tử là 1 word có word chứa ký tự trong EditTex
					ArrayList<String> temp=new ArrayList<String>();
					ArrayList<String> temp1=new ArrayList<String>();
					ArrayList<String> temp2=new ArrayList<String>();

					int textlength = editsearch.getText().length();
					temp.clear();
					for (int i = 0; i < tonghop.size(); i++)
					{
						if (textlength <= tonghop.get(i).length())
						{
							if(editsearch.getText().toString().equalsIgnoreCase(
									(String)
									tonghop.get(i).subSequence(0,
											textlength)))
							{
								temp.add(tonghop.get(i));
								temp1.add(nghia.get(i));
								temp2.add(vidu.get(i));
							}
						}
					}
					lv.setAdapter(new RegularArrayAdapter(getActivity(),temp,temp1,temp2 ));
					
				}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

		
		
		
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent intent =new Intent(getActivity().getApplicationContext(), Activity_Regular_View.class);
				intent.putExtra("thanhcs", position);
				startActivity(intent);
				
			}
			
			
		});
		
		
		return rootView;
	}
	

	
}
