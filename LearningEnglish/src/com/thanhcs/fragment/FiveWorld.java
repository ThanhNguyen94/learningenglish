package com.thanhcs.fragment;

import java.util.ArrayList;
import java.util.Dictionary;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.thanhcs.adapter.FiveWorldArrayAdapter;
import com.thanhcs.adapter.PharseArrayAdapter;
import com.thanhcs.database.PharseDataBaseProcess;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;


public class FiveWorld extends Fragment{

	ListView lv;
	ArrayList<Word> arrWord = new ArrayList<Word>();
	Word [] word;
	Word myWord = new Word();
	ProcessWordDataBase db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.five_world, container, false);

		lv =(ListView)rootView.findViewById(R.id.lv);
		db =  new ProcessWordDataBase(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();

		word = new Word[5];
		int []a = new int[5];
		int so = (int)(Math.random()*500);
		a[0]=5;a[1]=300;a[2]=1000;a[3]=1500;a[4]=2500;
		for(int i=0;i<5;i++)
		{
			a[i]=a[i]+so;
			word[i] = db.getTestDataWord(a[i]);
			arrWord.add(word[i]);
		}

		FiveWorldArrayAdapter adt;
		adt = new FiveWorldArrayAdapter(getActivity(),arrWord);
		lv.setAdapter(adt);

		return rootView;
	}
	
	
	@Override
	public void onDestroy() {
		db.close();
		super.onDestroy();
	}
}
