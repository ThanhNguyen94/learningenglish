package com.thanhcs.learning3000english;

import java.util.ArrayList;
import java.util.Random;

import com.thanhcs.adapter.ArrayAdapterSpiner;
import com.thanhcs.adapter.PharseArrayAdapter;
import com.thanhcs.database.PharseDataBaseProcess;
import com.thanhcs.database.Phrases;
import com.thanhcs.database.PhrasesDatabaseHelper;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.learning3000english.*;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.CalendarContract.Colors;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ViewWordDetail extends Fragment implements OnClickListener{

	MediaPlayer mPlayer;
	TextView cauhoi;
	TextView dA, dB, dC, dD, tvtime;
	int a, b, c, d;
	ArrayList<Phrases> arrphra;
	Spinner spinner;
	int color;
	Context context;
	ArrayList<Phrases> myPhrases2 = new ArrayList<Phrases>();
	ArrayList<Phrases> myPhrases = new ArrayList<Phrases>();
	PharseDataBaseProcess<Phrases> db;
	int time =10, i=1;
	int diem;
	String temp;
	String  [] titlecntd;
	 Animation animTranslate ;
	 Animation animAlpha;
	 Animation animScale;
	 Animation animRotate;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.viewworddetail, container, false);
	
		animTranslate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_translate);
		 animAlpha = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_alpha);
		animScale = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_scale2);
		animRotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_rotate);
		cauhoi = (TextView) rootView.findViewById(R.id.tvcautu);
		dA = (TextView)rootView.findViewById(R.id.tvA);
		dB = (TextView)rootView.findViewById(R.id.tvB);
		dC = (TextView)rootView.findViewById(R.id.tvC);
		dD = (TextView)rootView.findViewById(R.id.tvD);
		tvtime = (TextView)rootView.findViewById(R.id.tvgio);
		spinner = (Spinner)rootView.findViewById(R.id.spinner1);
		dA.setOnClickListener(ViewWordDetail.this);
		dB.setOnClickListener(ViewWordDetail.this);
		dC.setOnClickListener(ViewWordDetail.this);
		dD.setOnClickListener(ViewWordDetail.this);


		db =  new PharseDataBaseProcess(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();
		myPhrases = db.getListPhrases();

		setcauhoi();
		setdropdown();
		return rootView;
	}



	private void setdropdown() {
		
		titlecntd = getResources().getStringArray(R.array.titlecntd);
		ArrayAdapterSpiner adp = new ArrayAdapterSpiner(getActivity().getApplicationContext(),titlecntd);
		adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adp);
		spinner.setOnItemSelectedListener((OnItemSelectedListener) new ProcessforSpinner());
		
	}



	private void setcauhoi() {
	
		dA.setBackgroundColor(getResources().getColor(R.color.background));
		dB.setBackgroundColor(getResources().getColor(R.color.background));
		dC.setBackgroundColor(getResources().getColor(R.color.background));
		dD.setBackgroundColor(getResources().getColor(R.color.background));
		int randomNum = 1 + (int)(Math.random()*4); 
		a = 1 + (int)(Math.random()*myPhrases.size());
		b = 1 + (int)(Math.random()*100);
		c = 100 + (int)(Math.random()*100);
		d = 200 + (int)(Math.random()*100);
		i=0;
		
		cauhoi.setText(myPhrases.get(a).getEnglish());
	
		cauhoi.startAnimation(animTranslate);
		dA.startAnimation(animTranslate);
		dB.startAnimation(animTranslate);
		dC.startAnimation(animTranslate);
		dD.startAnimation(animTranslate);
		if(randomNum==1)
		{
			dA.setText(myPhrases.get(a).getVietnamese());
			dB.setText(myPhrases.get(c).getVietnamese());
			dC.setText(myPhrases.get(b).getVietnamese());
			dD.setText(myPhrases.get(d).getVietnamese());
		}
		if(randomNum==2)
		{
			dA.setText(myPhrases.get(d).getVietnamese());
			dB.setText(myPhrases.get(a).getVietnamese());
			dC.setText(myPhrases.get(c).getVietnamese());
			dD.setText(myPhrases.get(b).getVietnamese());
		}
		if(randomNum==3)
		{
			dA.setText(myPhrases.get(b).getVietnamese());
			dB.setText(myPhrases.get(d).getVietnamese());
			dC.setText(myPhrases.get(a).getVietnamese());
			dD.setText(myPhrases.get(c).getVietnamese());
		}
		if(randomNum==4)
		{
			dA.setText(myPhrases.get(c).getVietnamese());
			dB.setText(myPhrases.get(b).getVietnamese());
			dC.setText(myPhrases.get(d).getVietnamese());
			dD.setText(myPhrases.get(a).getVietnamese());
		}

	}

	private void setkhitraloidung()
	{
		
		int ran = 1 + (int)(Math.random()*4); 
		if(ran==1)
		{
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.congattulaysan);
				
		}
		if(ran ==2)
		{
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.clever);
			
		}if(ran==3)
		{
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.fantasic);
			
		}
		if(ran ==4)
		{
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.thatgreat);
			
		}
		if(ran==5)
		{
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.waytogo);
		}
		mPlayer.start();
		diem++;
		tvtime.setText(""+diem);
		tvtime.startAnimation(animScale);
		if(myPhrases.size() >900)
		{
			setcauhoi();
		}
		else
		{
			setcauhoi2();
		}
		
	}

	private void setkhitraloisai()
	{
		int ran = 1 + (int)(Math.random()*1); 
		if(ran==1)
		{
			
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.tryanotheranswer);
			
		}
		if(ran ==2)
		{
			mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.sorry);
				
		}
		
		mPlayer.start();
		diem=0;
		tvtime.setText(""+diem);
}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvA:
			if(dA.getText().equals(myPhrases.get(a).getVietnamese()))
			{
				
				setkhitraloidung();

			}
			else
			{
				setkhitraloisai();
				dA.setBackgroundColor(getResources().getColor(R.color.sai));
				
			}
			break;
		case R.id.tvB:
			if(dB.getText().equals(myPhrases.get(a).getVietnamese()))
			{
				
				setkhitraloidung();

			}
			else
			{
				
				setkhitraloisai();
				dB.setBackgroundColor(getResources().getColor(R.color.sai));
			}
			break;
		case R.id.tvC:
			if(dC.getText().equals(myPhrases.get(a).getVietnamese()))
			{
				
				setkhitraloidung();
			}
			else
			{
				setkhitraloisai();
				dC.setBackgroundColor(getResources().getColor(R.color.sai));
				
			}
			break;
		case R.id.tvD:
			if(dD.getText().equals(myPhrases.get(a).getVietnamese()))
			{
				setkhitraloidung();
				
			
			}
			else
			{
				setkhitraloisai();
				dD.setBackgroundColor(getResources().getColor(R.color.sai));
				
			}
			break;

		default:
			break;
		}
	}
	private class  ProcessforSpinner implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			Toast.makeText(getActivity().getApplicationContext(), ""+titlecntd[arg2], Toast.LENGTH_SHORT).show();
			switch (arg2) {
			case 0:
			
				break;
			case 1:
				getloaicntd("nhungthanhnguthongdung");
				
				break;
			case 2:
				getloaicntd("chaohoi");
				
				
				break;
			case 3:
				getloaicntd("dulichphuonghuong");
				
				break;
			case 4:
				getloaicntd("consotienbac");
				
				break;
			case 5:
				getloaicntd("diadiem");
				
				break;
			case 6:
				getloaicntd("dienthoaiinternetthu");
			
				break;
			case 7:
				getloaicntd("thoigianvangaythang");
				
				break;
			case 8:
				getloaicntd("choano");
			
				break;
			case 9:
				getloaicntd("an");
			
				break;
			case 10:
				getloaicntd("ketban");
				
				break;
			case 11:
				getloaicntd("giaitri");
			 
				break;
			case 12:
				getloaicntd("muasam");
				
				break;
			case 13:
				getloaicntd("khokhangiaotiep");
				
				break;
			case 14:
				getloaicntd("truonghopkhancapsuckhoe");
				
				break;
			case 15:
				getloaicntd("cauhoithongdung");
				
				break;
			case 16:
				getloaicntd("vieclam");
				
				break;
			case 17:
				getloaicntd("thoitiet");
				
				break;



			default:
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}
		
		public void getloaicntd(String tag) {
			
			myPhrases= db.getcntdloai(tag);
			myPhrases2 = db.getListPhrases();
			a = 1 + (int)(Math.random()*myPhrases.size());
			b = 1 + (int)(Math.random()*100);
			c = 100 + (int)(Math.random()*100);
			d = 200 + (int)(Math.random()*100);
			i=0;
			setcauhoi2();
			

		}		
	}
	private void setcauhoi2() {
		
		dA.setBackgroundColor(getResources().getColor(R.color.background));
		dB.setBackgroundColor(getResources().getColor(R.color.background));
		dC.setBackgroundColor(getResources().getColor(R.color.background));
		dD.setBackgroundColor(getResources().getColor(R.color.background));
		int randomNum = 1 + (int)(Math.random()*4); 
		a = 1 + (int)(Math.random()*myPhrases.size());
		b = 1 + (int)(Math.random()*100);
		c = 100 + (int)(Math.random()*100);
		d = 200 + (int)(Math.random()*100);
		i=0;
		
		cauhoi.setText(myPhrases.get(a).getEnglish());
		if(randomNum==1)
		{
			dA.setText(myPhrases.get(a).getVietnamese());
			dB.setText(myPhrases2.get(c).getVietnamese());
			dC.setText(myPhrases2.get(b).getVietnamese());
			dD.setText(myPhrases2.get(d).getVietnamese());
		}
		if(randomNum==2)
		{
			dA.setText(myPhrases2.get(d).getVietnamese());
			dB.setText(myPhrases.get(a).getVietnamese());
			dC.setText(myPhrases2.get(c).getVietnamese());
			dD.setText(myPhrases2.get(b).getVietnamese());
		}
		if(randomNum==3)
		{
			dA.setText(myPhrases2.get(b).getVietnamese());
			dB.setText(myPhrases2.get(d).getVietnamese());
			dC.setText(myPhrases.get(a).getVietnamese());
			dD.setText(myPhrases2.get(c).getVietnamese());
		}
		if(randomNum==4)
		{
			dA.setText(myPhrases2.get(c).getVietnamese());
			dB.setText(myPhrases2.get(b).getVietnamese());
			dC.setText(myPhrases2.get(d).getVietnamese());
			dD.setText(myPhrases.get(a).getVietnamese());
		}

	}
	
}
