package com.thanhcs.learning3000english;

import java.util.ArrayList;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Regular;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thanhcs.database.*;

public class Activity_Regular_View extends Activity {

	TextView tv1, tv2, tv3, tv4;
	ImageButton bt1, bt2, bt3, bt4;
	ProcessWordDataBase db;
	Context context;
	private AdView adView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewregular);
		adView = (AdView)findViewById(R.id.adView);
		adView.loadAd(new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)      
	    .addTestDevice("85AFA7708F0526A57D3599F39F71656D").build());
		tv1 = (TextView)findViewById(R.id.tv1);
		tv2 = (TextView)findViewById(R.id.tv2);
		tv3 = (TextView)findViewById(R.id.tv3);
		tv4 = (TextView)findViewById(R.id.tvvidu123);

		bt1 =(ImageButton)findViewById(R.id.speak);

		bt2 =(ImageButton)findViewById(R.id.speak2);

		bt3 =(ImageButton)findViewById(R.id.speak3);

		bt4 =(ImageButton)findViewById(R.id.speak4);
		Intent intent = getIntent();
		int i = intent.getIntExtra("thanhcs", 0);

		db = new ProcessWordDataBase(Activity_Regular_View.this);
		db.open();
		ArrayList<Regular> regular = db.getRegular();

		tv1.setText(regular.get(i).getInfinitive());
		tv2.setText(regular.get(i).getPast_simple());
		tv3.setText(regular.get(i).getPast_paticiple());
		tv4.setText(regular.get(i).getExample());

		bt1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new com.thanhcs.database.TTS(tv1.getText().toString(),Activity_Regular_View.this);
				
			}
		});

		bt2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new com.thanhcs.database.TTS(tv2.getText().toString(),Activity_Regular_View.this);
				
			}
		});
		bt3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new com.thanhcs.database.TTS(tv3.getText().toString(),Activity_Regular_View.this);
				
			}
		});
		bt4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new com.thanhcs.database.TTS(tv4.getText().toString(),Activity_Regular_View.this);
				
			}
		});
	}
	@Override
	public void onPause() {
        adView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    public void onDestroy() {
        adView.destroy();
        super.onDestroy();
    }
}
