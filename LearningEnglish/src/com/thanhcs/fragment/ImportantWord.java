package com.thanhcs.fragment;

import java.util.ArrayList;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thanhcs.learning3000english.*;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.thanhcs.adapter.FullWordArrayAdapter;
import com.thanhcs.adapter.TuDaHocArrayAdapter;
import com.thanhcs.database.*;

public class ImportantWord extends Fragment {
	ArrayList<Word> arrWord =new ArrayList<Word>();
	ProcessWordDataBase db;
	ListView lv;
	private AdView adView;
	EditText editsearch;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.importantword, container, false);
		adView = (AdView)rootView.findViewById(R.id.adView);
		adView.loadAd(new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)      
	    .addTestDevice("85AFA7708F0526A57D3599F39F71656D").build());

		
		lv =(ListView) rootView.findViewById(R.id.lvlearn);
		editsearch = (EditText)rootView.findViewById(R.id.textView1);
		TextView tv2 =(TextView)rootView.findViewById(R.id.textView2);
		tv2.setVisibility(rootView.INVISIBLE);
		db=  new ProcessWordDataBase(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();
		arrWord =  db.getTestImportant();

		TuDaHocArrayAdapter adt; 
		adt = new TuDaHocArrayAdapter(getActivity(), arrWord);
		lv.setAdapter(adt);
		editsearch.addTextChangedListener(new TextWatcher() {
			//Event when changed word on EditTex
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//Lọc  array list voi nhưng phần tử là 1 word có word chứa ký tự trong EditTex
					ArrayList<Word> temp=new ArrayList<Word>();
					int textlength = editsearch.getText().length();
					temp.clear();
					for (int i = 0; i < arrWord.size(); i++)
					{
						if (textlength <= arrWord.get(i).getWord().length())
						{
							if(editsearch.getText().toString().equalsIgnoreCase(
									(String)
									arrWord.get(i).getWord().subSequence(0,
											textlength)))
							{
								temp.add(arrWord.get(i));
							}
						}
					}
					lv.setAdapter(new FullWordArrayAdapter(getActivity(),temp));
					
				}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

		return rootView;
	}
	@Override
	  public void onResume() {
	    super.onResume();
	    if (adView != null) {
	      adView.resume();
	    }
	  }

	  @Override
	  public void onPause() {
	    if (adView != null) {
	      adView.pause();
	    }
	    super.onPause();
	  }

	  /** Called before the activity is destroyed. */
	  @Override
	  public void onDestroy() {
	    // Destroy the AdView.
		  db.close();
	    if (adView != null) {
	      adView.destroy();
	    }
	    super.onDestroy();
	  }
	
}
