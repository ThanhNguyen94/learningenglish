package com.thanhcs.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thanhcs.database.Phrases;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;
public class Grammar_Adapter extends ArrayAdapter<String> {

	public ArrayList<String> dong1;
	public ArrayList<String> dong2;
	public Context context;
	ArrayList<Phrases>myPhrases;
	public LayoutInflater inflater;
	TextView dong_1, dong_2;


	public Grammar_Adapter(Context context2,
		
			ArrayList<String>  dong1,
			ArrayList<String>  dong2) {
		super(context2, R.layout.grammar_list, dong1);
		this.context = context2;
		this.dong1 =dong1;
		this.dong2 = dong2;


		this.inflater = (LayoutInflater)context2.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public Context getContext() {
		return super.getContext();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView = inflater.inflate(R.layout.grammar_list, null);
		dong_1 = (TextView) convertView.findViewById(R.id.firstline);
		dong_2= (TextView) convertView.findViewById(R.id.secondLine);
		dong_1.setText(dong1.get(position));
		dong_2.setText(dong2.get(position));
		
		
		
		return convertView;
	}

}
