package com.thanhcs.database;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class PharseDataBaseProcess<DataBaseHelper>   {

	protected static final String TAG = "DataAdapter"; 

	private final Context mContext; 
	private SQLiteDatabase mDb; 
	private  PhrasesDatabaseHelper mDbHelper; 
	public PharseDataBaseProcess (Context context)  
	{ 
		this.mContext = context; 
		mDbHelper = new PhrasesDatabaseHelper(mContext); 
	} 
	//khởi tạo 1 TestAdapter
	public PharseDataBaseProcess  createDatabase() throws SQLException  
	{ 
		try  
		{ 
			mDbHelper.createDataBase(); 
		}  
		catch (IOException mIOException)  
		{ 
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase"); 
			throw new Error("UnableToCreateDatabase"); 
		} 
		return this; 
	} 
	//Mở sau khi đã đươc khởi tạo
	public PharseDataBaseProcess  open() throws SQLException  
	{ 
		try  
		{ 
			mDbHelper.openDataBase(); 
			mDbHelper.close(); 
			mDb = mDbHelper.getWritableDatabase(); 
		}  
		catch (SQLException mSQLException)  
		{ 
			Log.e(TAG, "open >>"+ mSQLException.toString()); 
			throw mSQLException; 
		} 
		return this; 
	} 
	///close database
	public void close()  
	{ 
		mDbHelper.close(); 
	} 
	//Get all data from database
	public ArrayList<Phrases> getListPhrases() 
	{ 
		ArrayList<Phrases> list=new ArrayList<Phrases>();
		String sql ="SELECT *from phrases"; 
		Cursor mCur = mDb.rawQuery(sql, null); 
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			Phrases resut=new Phrases();
			resut.set_id(mCur.getInt(mCur.getColumnIndex("_id")));
			resut.setEnglish(mCur.getString(mCur.getColumnIndex("english")));
			resut.setVietnamese(mCur.getString(mCur.getColumnIndex("vietnamese")));
			resut.setFavorite(mCur.getString(mCur.getColumnIndex("favorite")));
			resut.setCatalog(mCur.getString(mCur.getColumnIndex("catalog")));
			resut.setUnsignvietnamese(mCur.getString(mCur.getColumnIndex("unsignvietnamese")));
			resut.setPathaudio(mCur.getString(mCur.getColumnIndex("pathaudio")));
			resut.setTag(mCur.getString(mCur.getColumnIndex("tag")));
			
			mCur.moveToNext();
		
			list.add(resut);
		}
		return list;
	}
	
	public ArrayList<Phrases> getcntdloai(String tag) 
	{ 
		ArrayList<Phrases> list=new ArrayList<Phrases>();
		String sql ="SELECT *from phrases where tag = "+"\'"+tag+"\'"; 
		Cursor mCur = mDb.rawQuery(sql, null); 
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			Phrases resut=new Phrases();
			resut.set_id(mCur.getInt(mCur.getColumnIndex("_id")));
			resut.setEnglish(mCur.getString(mCur.getColumnIndex("english")));
			resut.setVietnamese(mCur.getString(mCur.getColumnIndex("vietnamese")));
			resut.setFavorite(mCur.getString(mCur.getColumnIndex("favorite")));
			resut.setCatalog(mCur.getString(mCur.getColumnIndex("catalog")));
			resut.setUnsignvietnamese(mCur.getString(mCur.getColumnIndex("unsignvietnamese")));
			resut.setPathaudio(mCur.getString(mCur.getColumnIndex("pathaudio")));
			resut.setTag(mCur.getString(mCur.getColumnIndex("tag")));
			
			mCur.moveToNext();
		
			list.add(resut);
		}
		return list;
	}
	
	
	public Phrases getcntd(int id) 
	{ 
		ArrayList<Phrases> list=new ArrayList<Phrases>();
		String sql ="SELECT * FROM phrases where _id = "+id ;
		Cursor mCur = mDb.rawQuery(sql, null); 

			Phrases resut=new Phrases();
			resut.set_id(mCur.getInt(mCur.getColumnIndex("_id")));
			resut.setEnglish(mCur.getString(mCur.getColumnIndex("english")));
			resut.setVietnamese(mCur.getString(mCur.getColumnIndex("vietnamese")));
			resut.setFavorite(mCur.getString(mCur.getColumnIndex("favorite")));
			resut.setCatalog(mCur.getString(mCur.getColumnIndex("catalog")));
			resut.setUnsignvietnamese(mCur.getString(mCur.getColumnIndex("unsignvietnamese")));
			resut.setPathaudio(mCur.getString(mCur.getColumnIndex("pathaudio")));
			resut.setTag(mCur.getString(mCur.getColumnIndex("tag")));
	
		
		return resut;
	}
	
}
