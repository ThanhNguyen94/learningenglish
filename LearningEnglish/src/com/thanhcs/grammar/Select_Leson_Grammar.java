package com.thanhcs.grammar;

import java.util.ArrayList;

import com.thanhcs.adapter.Grammar_Adapter;

import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.thanhcs.learning3000english.*;

public class Select_Leson_Grammar extends Fragment {

	
	private String []titlegrammar;
	private String []titlegrammar2;
	ArrayList<String>argrammar;
	ArrayList<String>argrammar2;
	ListView lv;
	EditText editsearch;
	Spinner spinnerabc;
	Grammar_Adapter adp;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fullword, container, false);
		TextView tv2 =(TextView)rootView.findViewById(R.id.textView2);
		tv2.setVisibility(rootView.GONE);
		editsearch = (EditText)rootView.findViewById(R.id.textView1);
		editsearch.setVisibility(rootView.GONE);
		spinnerabc =(Spinner)rootView.findViewById(R.id.spiner);
		spinnerabc.setVisibility(rootView.GONE);
		argrammar = new ArrayList<String>();
		argrammar2 = new ArrayList<String>();
		lv =(ListView)rootView.findViewById(R.id.lv);
		titlegrammar = getResources().getStringArray(R.array.titlegrammar);
		titlegrammar2 = getResources().getStringArray(R.array.titlegrammar2);
		
		for(int  i=0;i< titlegrammar.length;i++)
		{
			argrammar.add(titlegrammar[i]);
			argrammar2.add(titlegrammar2[i]);
		}
		
		adp = new Grammar_Adapter(getActivity().getApplicationContext(), argrammar,argrammar2);
		
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			/* (non-Javadoc)
			 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
			 */
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Bundle bunde = new Bundle();
				bunde.putInt("so", arg2+1);
				Intent intent = new Intent(getActivity().getApplicationContext(), ViewGrammar.class);
				intent.putExtra("data", bunde);
				startActivity(intent);
				
			}
		
		
		});
		return rootView;
		
		
		
		
	}

}
