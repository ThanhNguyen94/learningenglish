package com.thanhcs.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;

public class FiveWorldArrayAdapter extends ArrayAdapter<Word> {


	public Activity context;
	ArrayList<Word>arrmyword;
	public LayoutInflater inflater;
	TextView tvnghia, tvloaitu, tvvidu;
	@SuppressWarnings("rawtypes")
	ProcessWordDataBase db;
	String textnutdahoc, wordstate;

	public FiveWorldArrayAdapter(Activity context,
			ArrayList<Word> arrmyword) {
		super(context, R.layout.fiveworld_item, arrmyword);
		this.context = context;
		this.arrmyword = arrmyword;



		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public Context getContext() {
		return super.getContext();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public View getView(final int position,View convertView, ViewGroup parent) {

		convertView = inflater.inflate(R.layout.fullworld_item, null);
		final Animation aniAlpa = AnimationUtils.loadAnimation(getContext(),
				R.anim.anim_alpha);
		final TextView tvtu, tvphatam;
		tvtu = (TextView) convertView.findViewById(R.id.firstline);
		tvloaitu = (TextView) convertView.findViewById(R.id.tvloaitu);
		tvnghia = (TextView) convertView.findViewById(R.id.secondLine);
		tvphatam =(TextView) convertView.findViewById(R.id.tvam);

		db =  new ProcessWordDataBase(getContext().getApplicationContext());
		db.createDatabase();
		db.open();

		tvtu.setText(arrmyword.get(position).getWord());
		tvtu.startAnimation(aniAlpa);
		tvnghia.setText(arrmyword.get(position).getVidu());
		tvphatam.setText(arrmyword.get(position).getSound());
		tvloaitu.setText(arrmyword.get(position).getType());


		final ImageView bthoc;
		bthoc = (ImageView)convertView.findViewById(R.id.bthocf);

		if(arrmyword.get(position).getLearn()==1)
		{
			bthoc.setImageResource(R.drawable.favorite);

		}
		else
		{
			bthoc.setImageResource(R.drawable.add_favorite);

		}

		bthoc.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {


				if(arrmyword.get(position).getLearn()==1)
				{	
					textnutdahoc = "Chưa học từ này";
					wordstate ="Loại từ "+arrmyword.get(position).getWord()+" khỏi danh sách từ đã học !";
				}
				else
				{	
					textnutdahoc = "Ok, Học xong từ này";
					wordstate ="Thêm "+arrmyword.get(position).getWord()+" vào danh sách từ đã học !";	
				}

				View convertView = inflater.inflate(R.layout.choosedahoc, null);
				{
					TextView tvtu, tvphatam,tvloaitu,tvnghia, tvgiaithich ;
					ImageButton speak;
					tvtu =(TextView)convertView.findViewById(R.id.firstlined);
					tvloaitu = (TextView) convertView.findViewById(R.id.tvloaitud);
					tvnghia = (TextView) convertView.findViewById(R.id.tvnghiae);
					tvphatam =(TextView) convertView.findViewById(R.id.tvamd);
					tvgiaithich =(TextView) convertView.findViewById(R.id.tvgiaithichd);
					speak = (ImageButton)convertView.findViewById(R.id.speakenglishd);
					tvtu.setText(arrmyword.get(position).getWord());
					tvnghia.setText(arrmyword.get(position).getVidu());
					tvphatam.setText(arrmyword.get(position).getSound());
					tvloaitu.setText(arrmyword.get(position).getType());
					tvgiaithich.setText(arrmyword.get(position).getMean());

					speak.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							new com.thanhcs.database.TTS(arrmyword.get(position).getWord(),context);

						}
					});

				}
				new AlertDialog.Builder(context) 

				.setNegativeButton(textnutdahoc, 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {

						Word temp = arrmyword.get(position);
						Toast.makeText(getContext().getApplicationContext(),wordstate , Toast.LENGTH_LONG).show();

						if(temp.getLearn()==1){
							temp.setLearn(0);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.add_favorite);
						}
						else if(temp.getLearn()==0){
							temp.setLearn(1);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.favorite);			
						}

					}
				})
				.setView(convertView)
				.setPositiveButton("Xem lại từ", 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {
						di.dismiss();
					}
				})
				.show(); 

			}

		});








		tvtu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(arrmyword.get(position).getLearn()==1)
				{	
					textnutdahoc = "Chưa học từ này";
					wordstate ="Loại từ "+arrmyword.get(position).getWord()+" khỏi danh sách từ đã học !";
				}
				else
				{	
					textnutdahoc = "Ok, Học xong từ này";
					wordstate ="Thêm "+arrmyword.get(position).getWord()+" vào danh sách từ đã học !";	
				}

				View convertView = inflater.inflate(R.layout.choosedahoc, null);
				{
					TextView tvtu, tvphatam,tvloaitu,tvnghia, tvgiaithich ;
					ImageButton speak;
					tvtu =(TextView)convertView.findViewById(R.id.firstlined);
					tvloaitu = (TextView) convertView.findViewById(R.id.tvloaitud);
					tvnghia = (TextView) convertView.findViewById(R.id.tvnghiae);
					tvphatam =(TextView) convertView.findViewById(R.id.tvamd);
					tvgiaithich =(TextView) convertView.findViewById(R.id.tvgiaithichd);
					speak = (ImageButton)convertView.findViewById(R.id.speakenglishd);
					tvtu.setText(arrmyword.get(position).getWord());
					tvnghia.setText(arrmyword.get(position).getVidu());
					tvphatam.setText(arrmyword.get(position).getSound());
					tvloaitu.setText(arrmyword.get(position).getType());
					tvgiaithich.setText(arrmyword.get(position).getMean());

					speak.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							new com.thanhcs.database.TTS(arrmyword.get(position).getWord(),context);

						}
					});

				}
				new AlertDialog.Builder(context) 

				.setNegativeButton(textnutdahoc, 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {

						Word temp = arrmyword.get(position);
						Toast.makeText(getContext().getApplicationContext(),wordstate , Toast.LENGTH_LONG).show();

						if(temp.getLearn()==1){
							temp.setLearn(0);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.add_favorite);
						}
						else if(temp.getLearn()==0){
							temp.setLearn(1);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.favorite);			
						}

					}
				})
				.setView(convertView)
				.setPositiveButton("Xem lại từ", 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {
						di.dismiss();
					}
				})
				.show(); 

			}
		});







		tvloaitu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(arrmyword.get(position).getLearn()==1)
				{	
					textnutdahoc = "Chưa học từ này";
					wordstate ="Loại từ "+arrmyword.get(position).getWord()+" khỏi danh sách từ đã học !";
				}
				else
				{	
					textnutdahoc = "Ok, Học xong từ này";
					wordstate ="Thêm "+arrmyword.get(position).getWord()+" vào danh sách từ đã học !";		
				}

				View convertView = inflater.inflate(R.layout.choosedahoc, null);
				{
					TextView tvtu, tvphatam,tvloaitu,tvnghia, tvgiaithich ;
					ImageButton speak;
					tvtu =(TextView)convertView.findViewById(R.id.firstlined);
					tvloaitu = (TextView) convertView.findViewById(R.id.tvloaitud);
					tvnghia = (TextView) convertView.findViewById(R.id.tvnghiae);
					tvphatam =(TextView) convertView.findViewById(R.id.tvamd);
					tvgiaithich =(TextView) convertView.findViewById(R.id.tvgiaithichd);
					speak = (ImageButton)convertView.findViewById(R.id.speakenglishd);
					tvtu.setText(arrmyword.get(position).getWord());
					tvnghia.setText(arrmyword.get(position).getVidu());
					tvphatam.setText(arrmyword.get(position).getSound());
					tvloaitu.setText(arrmyword.get(position).getType());
					tvgiaithich.setText(arrmyword.get(position).getMean());

					speak.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							new com.thanhcs.database.TTS(arrmyword.get(position).getWord(),context);

						}
					});

				}
				new AlertDialog.Builder(context) 

				.setNegativeButton(textnutdahoc, 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {

						Word temp = arrmyword.get(position);
						Toast.makeText(getContext().getApplicationContext(),wordstate , Toast.LENGTH_LONG).show();

						if(temp.getLearn()==1){
							temp.setLearn(0);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.add_favorite);
						}
						else if(temp.getLearn()==0){
							temp.setLearn(1);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.favorite);			
						}

					}
				})
				.setView(convertView)
				.setPositiveButton("Xem lại từ", 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {
						di.dismiss();
					}
				})
				.show(); 


			}
		});










		tvphatam.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(arrmyword.get(position).getLearn()==1)
				{	
					textnutdahoc = "Chưa học từ này";
					wordstate ="Loại từ "+arrmyword.get(position).getWord()+" khỏi danh sách từ đã học !";
				}
				else
				{	
					textnutdahoc = "Ok, Học xong từ này";
					wordstate ="Thêm "+arrmyword.get(position).getWord()+" vào danh sách từ đã học !";	
				}

				View convertView = inflater.inflate(R.layout.choosedahoc, null);
				{
					TextView tvtu, tvphatam,tvloaitu,tvnghia, tvgiaithich ;
					ImageButton speak;
					tvtu =(TextView)convertView.findViewById(R.id.firstlined);
					tvloaitu = (TextView) convertView.findViewById(R.id.tvloaitud);
					tvnghia = (TextView) convertView.findViewById(R.id.tvnghiae);
					tvphatam =(TextView) convertView.findViewById(R.id.tvamd);
					tvgiaithich =(TextView) convertView.findViewById(R.id.tvgiaithichd);
					speak = (ImageButton)convertView.findViewById(R.id.speakenglishd);
					tvtu.setText(arrmyword.get(position).getWord());
					tvnghia.setText(arrmyword.get(position).getVidu());
					tvphatam.setText(arrmyword.get(position).getSound());
					tvloaitu.setText(arrmyword.get(position).getType());
					tvgiaithich.setText(arrmyword.get(position).getMean());

					speak.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							new com.thanhcs.database.TTS(arrmyword.get(position).getWord(),context);

						}
					});

				}
				new AlertDialog.Builder(context) 

				.setNegativeButton(textnutdahoc, 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {

						Word temp = arrmyword.get(position);
						Toast.makeText(getContext().getApplicationContext(),wordstate , Toast.LENGTH_LONG).show();

						if(temp.getLearn()==1){
							temp.setLearn(0);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.add_favorite);
						}
						else if(temp.getLearn()==0){
							temp.setLearn(1);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.favorite);			
						}

					}
				})
				.setView(convertView)
				.setPositiveButton("Xem lại từ", 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {
						di.dismiss();
					}
				})
				.show(); 

			}
		});










		tvnghia.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(arrmyword.get(position).getLearn()==1)
				{	
					textnutdahoc = "Chưa học từ này";
					wordstate ="Loại từ "+arrmyword.get(position).getWord()+" khỏi danh sách từ đã học !";
				}
				else
				{	
					textnutdahoc = "Ok, Học xong từ này";
					wordstate ="Thêm "+arrmyword.get(position).getWord()+" vào danh sách từ đã học !";	
				}

				View convertView = inflater.inflate(R.layout.choosedahoc, null);
				{
					TextView tvtu, tvphatam,tvloaitu,tvnghia, tvgiaithich ;
					ImageButton speak;
					tvtu =(TextView)convertView.findViewById(R.id.firstlined);
					tvloaitu = (TextView) convertView.findViewById(R.id.tvloaitud);
					tvnghia = (TextView) convertView.findViewById(R.id.tvnghiae);
					tvphatam =(TextView) convertView.findViewById(R.id.tvamd);
					tvgiaithich =(TextView) convertView.findViewById(R.id.tvgiaithichd);
					speak = (ImageButton)convertView.findViewById(R.id.speakenglishd);
					tvtu.setText(arrmyword.get(position).getWord());
					tvnghia.setText(arrmyword.get(position).getVidu());
					tvphatam.setText(arrmyword.get(position).getSound());
					tvloaitu.setText(arrmyword.get(position).getType());
					tvgiaithich.setText(arrmyword.get(position).getMean());

					speak.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							new com.thanhcs.database.TTS(arrmyword.get(position).getWord(),context);

						}
					});

				}
				new AlertDialog.Builder(context) 

				.setNegativeButton(textnutdahoc, 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {

						Word temp = arrmyword.get(position);
						Toast.makeText(getContext().getApplicationContext(),wordstate , Toast.LENGTH_LONG).show();

						if(temp.getLearn()==1){
							temp.setLearn(0);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.add_favorite);
						}
						else if(temp.getLearn()==0){
							temp.setLearn(1);
							db.updatetWord(temp);//Update láº¡i Importan cá»§a word trong database sau khi sá»­a chá»¯a
							bthoc.setImageResource(R.drawable.favorite);			
						}

					}
				})
				.setView(convertView)
				.setPositiveButton("Xem lại từ", 
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface di, int what) {
						di.dismiss();
					}
				})
				.show(); 

			}
		});







		final ImageButton btspeak;
		btspeak = (ImageButton)convertView.findViewById(R.id.speakenglish);
		btspeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				tvphatam.startAnimation(aniAlpa);
				tvtu.startAnimation(aniAlpa);
				new com.thanhcs.database.TTS(arrmyword.get(position).getWord(),context);

			}

		});






		return convertView;
	}

}

