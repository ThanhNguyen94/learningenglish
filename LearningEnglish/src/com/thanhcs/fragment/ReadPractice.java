package com.thanhcs.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;


public class ReadPractice extends Fragment {

	int tocdodoc;
	TextView tvtu, tvnghia, tvvidu;
	Button bttudahoc,btngaunhien, bttocdo;
	int i=0;
	int indext=1;
	ProcessWordDataBase db ;
	ArrayList<Word> arrWord = new ArrayList<Word>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.autospeaking, container, false);
		final Animation animTranslate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_translate);
		final Animation animAlpha = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_alpha);
		final Animation animScale = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_scale2);
		final Animation animRotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_rotate);
		tvtu = (TextView)rootView.findViewById(R.id.tvtuvung);
		tvnghia = (TextView)rootView.findViewById(R.id.tvnghia2);
		tvvidu = (TextView)rootView.findViewById(R.id.tvvidu2);
		
		bttudahoc =(Button)rootView.findViewById(R.id.btdahoc);
		bttocdo =(Button)rootView.findViewById(R.id.btsettocdo);
		btngaunhien =(Button)rootView.findViewById(R.id.btngaunhien);
		 db =  new ProcessWordDataBase(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();
		tocdodoc =5;
	
		bttocdo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tocdodoc=1;
			}
		});
		
		bttudahoc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//============================
				arrWord = db.getTestImportant();
				Word testWord = arrWord.get(indext);


				CountDownTimer a = new CountDownTimer(100000, 1000) {

					@Override
					public void onTick(long millisUntilFinished) {
//						i++;
//						if(i%tocdodoc==0){
//							tvtu.startAnimation(animScale);
//							tvtu.setText(arrWord.get(indext).getWord());
//							tvnghia.setText(arrWord.get(indext).getVidu());
//							tvvidu.setText(arrWord.get(indext).getMean());
//							tvnghia.setAnimation(animAlpha);
//							tvnghia.setAnimation(animTranslate);
//							new com.thanhcs.database.TTS(arrWord.get(indext).getWord(), getActivity().getApplicationContext());	
//							
//							indext ++;
//						}
						Intent intent = new Intent(getActivity().getApplicationContext(), ViewWordDetail.class);
						startActivity(intent);

					}

					@Override
					public void onFinish() {
						// TODO Auto-generated method stub

					}
				};
				a.start();

				
			}
		});
		
		btngaunhien.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				arrWord = db.getListAbcWord(1, 3110);
				Word testWord = arrWord.get(indext);


				CountDownTimer a = new CountDownTimer(100000, 1000) {

					@Override
					public void onTick(long millisUntilFinished) {
						i++;
						if(i%5==0){
							tvtu.startAnimation(animScale);
							tvtu.setText(arrWord.get(indext).getWord());
							tvnghia.setText(arrWord.get(indext).getVidu());
							tvvidu.setText(arrWord.get(indext).getMean());
							tvnghia.setAnimation(animAlpha);
							tvnghia.setAnimation(animTranslate);
							new com.thanhcs.database.TTS(arrWord.get(indext).getWord(), getActivity().getApplicationContext());	
							
							indext ++;
						}

					}

					@Override
					public void onFinish() {
						// TODO Auto-generated method stub

					}
				};
				a.start();

				
				
			}
		});
		
		
		
		
		return rootView;

	}
	
}
