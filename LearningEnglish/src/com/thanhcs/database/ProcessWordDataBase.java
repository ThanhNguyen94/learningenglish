package com.thanhcs.database;

import java.io.IOException; 
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context; 
import android.database.Cursor; 
import android.database.SQLException; 
import android.database.sqlite.SQLiteDatabase; 
import android.util.Log; 
//Class chính thao tac1 với dữ liệu
public class ProcessWordDataBase<DataBaseHelper>  
{ 
	protected static final String TAG = "DataAdapter"; 

	private final Context mContext; 
	private SQLiteDatabase mDb; 
	private MyDataBaseHelper mDbHelper; 
	public ProcessWordDataBase (Context context)  
	{ 
		this.mContext = context; 
		mDbHelper = new MyDataBaseHelper(mContext); 
	} 
	//khởi tạo 1 TestAdapter
	public ProcessWordDataBase  createDatabase() throws SQLException  
	{ 
		try  
		{ 
			mDbHelper.createDataBase(); 
		}  
		catch (IOException mIOException)  
		{ 
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase"); 
			throw new Error("UnableToCreateDatabase"); 
		} 
		return this; 
	} 
	//Mở sau khi đã đươc khởi tạo
	public ProcessWordDataBase  open() throws SQLException  
	{ 
		try  
		{ 
			mDbHelper.openDataBase(); 
			mDbHelper.close(); 
			mDb = mDbHelper.getWritableDatabase(); 
		}  
		catch (SQLException mSQLException)  
		{ 
			Log.e(TAG, "open >>"+ mSQLException.toString()); 
			throw mSQLException; 
		} 
		return this; 
	} 
	///close database
	public void close()  
	{ 
		mDbHelper.close(); 
	} 
	//Get all data from database
	public ArrayList<Word> getTestData() 
	{ 
		ArrayList<Word> list=new ArrayList<Word>();
		String sql ="SELECT * FROM data where id < 100"; 
		Cursor mCur = mDb.rawQuery(sql, null); 
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			Word resut=new Word();
			resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
			resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
			resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
			resut.setType(mCur.getString(mCur.getColumnIndex("type")));
			resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
			resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
			resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
			resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
			resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
			mCur.moveToNext();
		//	System.out.println(resut.toString());
			list.add(resut);
		}
		return list;
	}
	//Get data voi word
	public Word getTestDataWord(int id) 
	{ 
		Word resut=new Word();
		String sql ="select * from "+"data"+" where learn != 1 and "+"id"+" = "+String.valueOf(id);//"SELECT id, word FROm data";  
		Cursor mCur = mDb.rawQuery(sql,null);

		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
		resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
		resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
		resut.setType(mCur.getString(mCur.getColumnIndex("type")));
		resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
		resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
		resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
		resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
		resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
	//	Log.d("GetWord", resut.toString());
		return resut;
	}
	public Word getWordSetNomarl(int id) 
	{ 
		Word resut=new Word();
		String sql ="select * from "+"data"+" where "+"id"+" = "+String.valueOf(id);//"SELECT id, word FROm data";  
		Cursor mCur = mDb.rawQuery(sql,null);

		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
		resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
		resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
		resut.setType(mCur.getString(mCur.getColumnIndex("type")));
		resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
		resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
		resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
		resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
		resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
		
		return resut;
	}
	
	
	//Get data Important
	public ArrayList<Word> getTestImportant() 
	{ 
		ArrayList<Word> list =new ArrayList<Word>();
		String sql ="SELECT * FROM data where learn =1";//"SELECT id, word FROm data"; 
		Cursor mCur = mDb.rawQuery(sql,null);
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			Word resut=new Word();
			
			resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
			resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
			resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
			resut.setType(mCur.getString(mCur.getColumnIndex("type")));
			resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
			resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
			resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
			resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
			resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
		//	System.out.print("Tu da hoc" + resut.toString());
			
			mCur.moveToNext();
			list.add(resut);
		}
		return list;
	}
	//Get data Important
		public ArrayList<Word> getListAbcWord(int a, int b) 
		{ 
			ArrayList<Word> list =new ArrayList<Word>();
			String sql ="SELECT * FROM data where id between "+ a +" and "+ b;//learn != 1 and  
			Cursor mCur = mDb.rawQuery(sql,null);
			if (mCur!=null) 
			{ 
				mCur.moveToNext(); 
			} 
			while(mCur.isAfterLast()==false){
				Word resut=new Word();
				resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
				resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
				resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
				resut.setType(mCur.getString(mCur.getColumnIndex("type")));
				resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
				resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
				resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
				resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
				resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
				mCur.moveToNext();
				list.add(resut);
			}
			return list;
		}
		
		
		//Get data Important
				public ArrayList<Word> getAllOfWord() 
				{ 
					ArrayList<Word> list =new ArrayList<Word>();
					String sql ="SELECT * FROM data"; 
					Cursor mCur = mDb.rawQuery(sql,null);
					if (mCur!=null) 
					{ 
						mCur.moveToNext(); 
					} 
					while(mCur.isAfterLast()==false){
						Word resut=new Word();
						resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
						resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
						resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
						resut.setType(mCur.getString(mCur.getColumnIndex("type")));
						resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
						resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
						resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
						resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
						resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
						mCur.moveToNext();
						list.add(resut);
					}
					return list;
				}


	// Get data Learn
	public ArrayList<Word> getTestLearn(int id) 
	{ 
		ArrayList<Word> list=new ArrayList<Word>();
		String sql ="select * from "+"data"+" where "+"learn"+" = "+String.valueOf(id);//"SELECT id, word FROm data"; 
		Cursor mCur = mDb.rawQuery(sql,null);
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			Word resut=new Word();
			resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
			resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
			resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
			resut.setType(mCur.getString(mCur.getColumnIndex("type")));
			resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
			resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
			resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
			resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
			resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
			mCur.moveToNext();
		//	System.out.print("Tu da hoc" + resut.toString());
			list.add(resut);
		}
		return list;
	}
	//Get data History
	public ArrayList<Word> getTestHistory(int id) 
	{ 
		ArrayList<Word> list=new ArrayList<Word>();
		String sql ="select * from "+"data"+" where "+"history"+" = "+String.valueOf(id);//"SELECT id, word FROm data"; 
		Cursor mCur = mDb.rawQuery(sql,null);
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			Word resut=new Word();
			resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
			resut.setWord(mCur.getString(mCur.getColumnIndex("word")));
			resut.setMean(mCur.getString(mCur.getColumnIndex("mean")));
			resut.setType(mCur.getString(mCur.getColumnIndex("type")));
			resut.setSound(mCur.getString(mCur.getColumnIndex("sound")));
			resut.setVidu(mCur.getString(mCur.getColumnIndex("vidu")));
			resut.setLearn(mCur.getInt(mCur.getColumnIndex("learn")));
			resut.setHistory(mCur.getInt(mCur.getColumnIndex("history")));
			resut.setImportan(mCur.getInt(mCur.getColumnIndex("importan")));
			mCur.moveToNext();
			list.add(resut);
		}
		return list;
	}
	//list lấy ra danh sách cách từ
	public ArrayList<String> getTestWord() 
	{ 
		ArrayList<String> list=new ArrayList<String>();
		String sql ="SELECT * FROM data where learn = 1"; 
		Cursor mCur = mDb.rawQuery(sql, null); 
		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		while(mCur.isAfterLast()==false){
			String s="";
			s=""+mCur.getString(mCur.getColumnIndex("word"));
			mCur.moveToNext();
	//		System.out.print("Tu da hoc" + s);
			list.add(s);
		}
		return list;
	}
	//update 1 từ vào database
	public void updatetWord(Word p) 
	{ 
		ContentValues values = new ContentValues();
		values.put("id",p.getId());
		values.put("word", p.getWord());
		values.put("type", p.getType());
		values.put("sound",p.getSound());
		values.put("mean",p.getMean());
		values.put("vidu",p.getVidu());
		values.put("learn",p.getLearn());
		values.put("history",p.getHistory());
		values.put("importan",p.getImportan());

		mDb.update("data", values,"id"+" = "+p.getId(), null);
		Log.d("GetWordsetnomarl", "update thanh cong" +p.getWord()+"Learn = "+p.getLearn());
		
	}
	//get all database from table irregular
		public ArrayList<Regular> getRegular(){
			ArrayList<Regular> list=new ArrayList<Regular>();
			String sql ="SELECT * FROM irregular"; 
			Cursor mCur = mDb.rawQuery(sql, null); 
			if (mCur!=null) 
			{ 
				mCur.moveToNext(); 
			} 
			while(mCur.isAfterLast()==false){
				Regular resut=new Regular();
				resut.setId(mCur.getInt(mCur.getColumnIndex("_id")));
				resut.setInfinitive(mCur.getString(mCur.getColumnIndex("infinitive")));
				resut.setPast_simple(mCur.getString(mCur.getColumnIndex("past_simple")));
				resut.setPast_paticiple(mCur.getString(mCur.getColumnIndex("past_paticiple")));
				resut.setVi_content(mCur.getString(mCur.getColumnIndex("vi_content")));
				resut.setExample(mCur.getString(mCur.getColumnIndex("example")));
				mCur.moveToNext();
				
				list.add(resut);
			}
			return list;
		}
} 

