package com.thanhcs.fragment;

import java.util.ArrayList;
import java.util.Random;

import android.app.Fragment;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;

public class TracNghiemTuVung extends Fragment {



	EditText ketqua;
	ImageButton btspeak;
	Button btcheck;
	ImageView imgv;
	Button btnext;
	TextView explain, tvchitiet;
	MediaPlayer mPlayer;
	Word testWord = new Word();
	ArrayList<Word> arrWord = new ArrayList<Word>();
	int indext = 1;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.tracnghiemtuvung, container, false);
		btspeak = (ImageButton) rootView.findViewById(R.id.btspeak);
		btcheck = (Button) rootView.findViewById(R.id.btcheckdapan);
		ketqua  = (EditText)rootView.findViewById(R.id.edketqua);
		explain = (TextView) rootView.findViewById(R.id.tvhuongdan);
	
		btnext = (Button) rootView.findViewById(R.id.btnext);
		//=================get database====================
		ProcessWordDataBase db =  new ProcessWordDataBase(getActivity().getApplicationContext());
		db.createDatabase();
		db.open();
		//============================
		arrWord = db.getTestImportant();

		testWord = arrWord.get(1);
		imgv = (ImageView)rootView.findViewById(R.id.imageView2);
		final Animation animTranslate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_translate);
		final Animation animAlpha = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_alpha);
		final Animation animScale = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_scale);
		final Animation animRotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
				R.anim.anim_rotate);



		btnext.setVisibility(View.GONE);

		btcheck.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View view) {
				String text = ketqua.getText().toString();

				if(testWord.getWord().equalsIgnoreCase(text))
				{


					imgv.setImageResource(R.drawable.dung);
					imgv.startAnimation(animRotate);
					mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.congattulaysan);
					mPlayer.start();
					btnext.setVisibility(View.VISIBLE);
					tvchitiet.setText("->> Xem Chi Tiết <<-");
					tvchitiet.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
//							Intent intent= new Intent(getActivity().getApplicationContext().getApplicationContext(), ViewWordDetail.class);
//							Bundle bundle = new Bundle();
//							bundle.putString("tu", testWord.getWord());
//							bundle.putString("loai", testWord.getType());
//							bundle.putString("nghia", testWord.getVidu());
//							bundle.putString("vidu", testWord.getMean());
//							bundle.putString("phatam", testWord.getSound());
//							intent.putExtra("data",bundle);
//							Toast.makeText(getActivity().getApplicationContext(), testWord.getWord(), Toast.LENGTH_SHORT).show();
//							getActivity().startActivity(intent);
							//	Toast.makeText(getActivity().getApplicationContext(), testWord.getWord(), Toast.LENGTH_SHORT).show();

						}
					});

				}
				else 
				{
					imgv.setImageResource(R.drawable.sai);
					imgv.startAnimation(animScale);
					mPlayer = MediaPlayer.create (getActivity().getApplicationContext(),R.raw.tryanotheranswer);
					mPlayer.start();
				}

			}
		});

		btnext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				indext++;
				Toast.makeText(getActivity().getApplicationContext(), testWord.getWord(), Toast.LENGTH_SHORT).show();
				ketqua.setText("");
				tvchitiet.setText("");
				btnext.setVisibility(View.GONE);
				//	Random random = new Random();
				//	 int randomInteger = random.nextInt(arrWord.size());
				testWord =  arrWord.get(indext);
				if (indext==(arrWord.size()-1))
				{
					indext =1;
					Toast.makeText(getActivity().getApplicationContext(), "Báº¡n Ä‘Ã£ thá»±c hÃ nh háº¿t trong list cÃ¡c tá»« Ä‘Ã£ há»�c !CHÃšC Má»ªNG Báº N !!",Toast.LENGTH_LONG ).show();				}

			}
		});




		btspeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new com.thanhcs.database.TTS(arrWord.get(indext).getWord(),getActivity().getApplicationContext());
				Toast.makeText(getActivity().getApplicationContext(), testWord.getWord(), Toast.LENGTH_SHORT).show();
			}
		});




		return rootView;
	}


}
