package com.thanhcs.learning3000english;

import java.util.ArrayList;

import com.thanhcs.adapter.ArrayAdapterSpiner;
import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class Activity_SpeakRandom extends Activity {

	int tocdodoc;
	TextView tvtu, tvnghia, tvvidu;
	Button bttudahoc,btngaunhien;
	Spinner sptocdo;
	int i=0;
	int indext=1;
	ProcessWordDataBase db ;
	ArrayList<Word> arrWord = new ArrayList<Word>();
	String a[] = {"Tốc độ", "1","2","3","4","5","6","7","8","9","10"};
	int randomNum;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.autospeaking);
		
			final Animation animTranslate = AnimationUtils.loadAnimation(Activity_SpeakRandom.this,
					R.anim.anim_translate);
			final Animation animAlpha = AnimationUtils.loadAnimation(Activity_SpeakRandom.this,
					R.anim.anim_alpha);
			final Animation animScale = AnimationUtils.loadAnimation(Activity_SpeakRandom.this,
					R.anim.anim_scale2);
			final Animation animRotate = AnimationUtils.loadAnimation(Activity_SpeakRandom.this,
					R.anim.anim_rotate);
			tvtu = (TextView)findViewById(R.id.tvtuvung);
			tvnghia = (TextView)findViewById(R.id.tvnghia2);
			tvvidu = (TextView)findViewById(R.id.tvvidu2);

			bttudahoc =(Button)findViewById(R.id.btdahoc);
			sptocdo =(Spinner)findViewById(R.id.btsettocdo);
			btngaunhien =(Button)findViewById(R.id.btngaunhien);
			db =  new ProcessWordDataBase(Activity_SpeakRandom.this);
			db.createDatabase();
			db.open();
			tocdodoc =5;
	
			ArrayAdapterSpiner adpspin =new ArrayAdapterSpiner(this, a);
			adpspin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//R.layout.spinnerdemo
			sptocdo.setAdapter(adpspin);
			sptocdo.setOnItemSelectedListener(new ItemselectSP());


			bttudahoc.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					//============================
					arrWord = db.getTestImportant();
					randomNum = 1 + (int)(Math.random()*arrWord.size()-2); 
					Word testWord = arrWord.get(randomNum);
					CountDownTimer a = new CountDownTimer(10000000, 1000) {

						@Override
						public void onTick(long millisUntilFinished) {
														i++;
														if(i%tocdodoc==0){
															randomNum = 1 + (int)(Math.random()*arrWord.size()-2); 
															tvtu.startAnimation(animScale);
															tvtu.setText(arrWord.get(randomNum).getWord());
															tvnghia.setText(arrWord.get(randomNum).getVidu());
															tvvidu.setText(arrWord.get(randomNum).getMean());
															tvnghia.setAnimation(animAlpha);
															tvnghia.setAnimation(animTranslate);
															tvvidu.setAnimation(animAlpha);
															new com.thanhcs.database.TTS(arrWord.get(randomNum).getWord(), Activity_SpeakRandom.this);	
										//					Toast.makeText(Activity_SpeakRandom.this, ""+randomNum,Toast.LENGTH_SHORT).show();
														}
							

						}

						@Override
						public void onFinish() {
							// TODO Auto-generated method stub

						}
					};
					a.start();


				}
			});

			btngaunhien.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					arrWord = db.getListAbcWord(1, 3110);
					Word testWord = arrWord.get(randomNum);


					CountDownTimer a = new CountDownTimer(10000000, 1000) {

						@Override
						public void onTick(long millisUntilFinished) {
							i++;
							if(i%tocdodoc==0){
								randomNum = 1 + (int)(Math.random()*arrWord.size()-2); 
								tvtu.startAnimation(animScale);
								tvtu.setText(arrWord.get(randomNum).getWord());
								tvnghia.setText(arrWord.get(randomNum).getVidu());
								tvvidu.setText(arrWord.get(randomNum).getMean());
								tvnghia.setAnimation(animAlpha);
								tvnghia.setAnimation(animTranslate);
								tvvidu.setAnimation(animAlpha);
								new com.thanhcs.database.TTS(arrWord.get(randomNum).getWord(), Activity_SpeakRandom.this);	
							}

						}

						@Override
						public void onFinish() {
							// TODO Auto-generated method stub

						}
					};
					a.start();



				}
			});



	}
	private class ItemselectSP implements OnItemSelectedListener
	{

		/* (non-Javadoc)
		 * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {

			if(a[arg2].equalsIgnoreCase(a[0]))
			{
				tocdodoc =5;
			}
			if(a[arg2].equalsIgnoreCase(a[1]))
			{
				tocdodoc =1;
			}
			if(a[arg2].equalsIgnoreCase(a[2]))
			{
				tocdodoc =2;
			}
			if(a[arg2].equalsIgnoreCase(a[3]))
			{
				tocdodoc =3;
			}
			if(a[arg2].equalsIgnoreCase(a[4]))
			{
				tocdodoc =4;
			}
			if(a[arg2].equalsIgnoreCase(a[5]))
			{
				tocdodoc =5;
			}
			if(a[arg2].equalsIgnoreCase(a[6]))
			{
				tocdodoc =6;
			}
			if(a[arg2].equalsIgnoreCase(a[7]))
			{
				tocdodoc =7;
			}
			if(a[arg2].equalsIgnoreCase(a[8]))
			{
				tocdodoc =8;
			}
			if(a[arg2].equalsIgnoreCase(a[9]))
			{
				tocdodoc =9;
			}
			if(a[arg2].equalsIgnoreCase(a[10]))
			{
				tocdodoc =10;
			}
		}
	
	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	}
	@Override
	public void onDestroy() {
		db.close();
		super.onDestroy();
	}
				
	}
