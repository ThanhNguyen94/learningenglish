package com.thanhcs.database;

import java.util.Locale;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

public class TTS implements OnInitListener{
	private TextToSpeech mTts;
	private String message;
	public TTS(String message, Context context){
		this.message = message;
		mTts = new TextToSpeech(context, this);
		mTts.setLanguage(Locale.UK);
	}
	@Override
	 public void onInit(int i)
    {
    	  if (i==mTts.SUCCESS)
    	    {
    		  mTts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    	    }
    }

}
