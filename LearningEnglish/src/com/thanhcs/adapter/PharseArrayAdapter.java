package com.thanhcs.adapter;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thanhcs.database.Phrases;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.*;
public class PharseArrayAdapter extends ArrayAdapter<String> {

	public ArrayList<String> english;
	public ArrayList<String> vietnamese;
	public Activity context;
	ArrayList<Phrases>myPhrases;
	public LayoutInflater inflater;
	


	public PharseArrayAdapter(Activity context,
		
			ArrayList<String>  english,
			ArrayList<String>  vietnamese) {
		super(context, R.layout.phrases_item, english);
		this.context = context;
		this.english =english;
		this.vietnamese = vietnamese;


		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public Context getContext() {
		return super.getContext();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final TextView tvenlish, tvvietnamese;
		final Animation animTranslate = AnimationUtils.loadAnimation(getContext(),
				R.anim.anim_translate);
		final Animation animAlpha = AnimationUtils.loadAnimation(getContext(),
				R.anim.anim_alpha);
		final Animation animScale = AnimationUtils.loadAnimation(getContext(),
				R.anim.anim_scale2);
		final Animation animRotate = AnimationUtils.loadAnimation(getContext(),
				R.anim.anim_rotate);
		convertView = inflater.inflate(R.layout.phrases_item, null);
		tvenlish = (TextView) convertView.findViewById(R.id.firstline);
		tvvietnamese= (TextView) convertView.findViewById(R.id.secondLine);
		tvenlish.setText(english.get(position));
		tvvietnamese.setText(vietnamese.get(position));
		tvvietnamese.startAnimation(animAlpha);
		
		tvenlish.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new com.thanhcs.database.TTS(english.get(position),context);	
				
			}
		});
		
		final ImageButton btspeak;
		btspeak = (ImageButton)convertView.findViewById(R.id.speakenglish);
		btspeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new com.thanhcs.database.TTS(english.get(position),context);
				tvenlish.startAnimation(animAlpha);
				tvvietnamese.startAnimation(animAlpha);
			}

		});
		
		return convertView;
	}

}
