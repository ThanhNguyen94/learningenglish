package com.thanhcs.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.sax.RootElement;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thanhcs.database.ProcessWordDataBase;
import com.thanhcs.database.Word;
import com.thanhcs.learning3000english.R;

public class RegularArrayAdapter extends ArrayAdapter {


	public Activity context;
	ArrayList<String>tu;
	ArrayList<String>nghia;
	ArrayList<String>vidu;
	public LayoutInflater inflater;
	TextView tvnghia, tvtu, tvvidu;
	
	public RegularArrayAdapter(Activity context2,
			ArrayList<String> tu2,
			ArrayList<String> nghia2,
			ArrayList<String> vidu2) {
		super(context2, R.layout.item_regular,tu2);
		this.context = context2;
		this.tu = tu2;
		this.nghia = nghia2;
		this.vidu = vidu2;



		this.inflater = (LayoutInflater)context2.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public Context getContext() {
		return super.getContext();
	}

	
	@Override
	public View getView(final int position,View convertView, ViewGroup parent) {

		convertView = inflater.inflate(R.layout.item_regular, null);
		final Animation aniAlpa = AnimationUtils.loadAnimation(getContext(),
				R.anim.anim_alpha);
		tvtu =(TextView) convertView.findViewById(R.id.tvregular);
		tvnghia =(TextView)convertView.findViewById(R.id.tvnghia);
		tvvidu =(TextView)convertView.findViewById(R.id.tvvidu);
		tvtu.startAnimation(aniAlpa);
		tvtu.setText(tu.get(position));
		tvnghia.setText(nghia.get(position));
		tvvidu.setText(vidu.get(position));
		
		return convertView;
	
	}
}
