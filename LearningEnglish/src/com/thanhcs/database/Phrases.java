package com.thanhcs.database;

public class Phrases {
	int _id;
	String english;
	String vietnamese;
	String pathaudio;
	String favorite;
	String catalog;
	String tag;
	String unsignvietnamese;
	
	public Phrases()
	{
		
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getEnglish() {
		return english;
	}

	public void setEnglish(String english) {
		this.english = english;
	}

	public String getVietnamese() {
		return vietnamese;
	}

	public void setVietnamese(String vietnamese) {
		this.vietnamese = vietnamese;
	}

	public String getPathaudio() {
		return pathaudio;
	}

	public void setPathaudio(String pathaudio) {
		this.pathaudio = pathaudio;
	}

	public String getFavorite() {
		return favorite;
	}

	public void setFavorite(String favorite) {
		this.favorite = favorite;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getUnsignvietnamese() {
		return unsignvietnamese;
	}

	public void setUnsignvietnamese(String unsignvietnamese) {
		this.unsignvietnamese = unsignvietnamese;
	}
	
	 @Override
	public String toString() {
		// TODO Auto-generated method stub
		return (english +"  " + vietnamese+"\n");
	}
	
}
