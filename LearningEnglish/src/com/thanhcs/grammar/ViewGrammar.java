package com.thanhcs.grammar;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.webkit.WebView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thanhcs.learning3000english.*;

public class ViewGrammar extends Activity {

	WebView mywebview;
	private AdView adView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_grammar);

		adView = (AdView)findViewById(R.id.adView);
				adView.loadAd(new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)      
			    .addTestDevice("85AFA7708F0526A57D3599F39F71656D").build());

		
		mywebview = (WebView)findViewById(R.id.webView1);
		Intent intent =getIntent();
		Bundle bunde = new Bundle();
		bunde = intent.getBundleExtra("data");
		int so = bunde.getInt("so");
		
		mywebview.loadUrl("file:///android_asset/"+so+".htm");
		
		
	}
	
	@Override
	public void onPause() {
        adView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    public void onDestroy() {
        adView.destroy();
        super.onDestroy();
    }

}
